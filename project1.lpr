program project1;
const
  MatrixSize = 6;
  MaxElement = 21;
  MinElement = -16;

type
  MatrixType = Array[1..MatrixSize, 1..MatrixSize] of Integer;
  Coordinates = Array[1..2] of Integer;


procedure CreateMatrix(var matrix: MatrixType);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            matrix[i, j] := Random(MaxElement - MinElement + 1) + MinElement;
        end;
    end;
end;

procedure WriteMatrix(var matrix: MatrixType);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            Write(matrix[i, j]:5);
        end;
        WriteLn;
    end;
    WriteLn;
end;

procedure CheckElements(var element1,element2, maxI, maxJ: Integer; i, j: Integer);
begin
    if element1 < element2 then
    begin
        element1 := element2;
        maxI := i;
        maxJ := j;
    end;
end;

procedure FindMaxElementOnMainDiagonal(var matrix: MatrixType; var coordinate: Coordinates);
var
    i, maxElement: Integer;
begin
    maxElement := matrix[1,1];
    for i := 1 to MatrixSize do
    begin
        CheckElements(maxElement, matrix[i, i], coordinate[1], coordinate[2], i, i);
    end;
    WriteLn('max element on main diagonal =', maxElement, ' i=', coordinate[1], ' j=', coordinate[2]);
end;

procedure FindMaxElementBelowMainDiagonal(var matrix: MatrixType; var coordinate: Coordinates);
var i, j, maxElement: Integer;
begin
    maxElement := matrix[2,1];
    for i := 3 to MatrixSize do
    begin
        for j := 1 to i - 1 do
        begin
            CheckElements(maxElement, matrix[i, j], coordinate[1], coordinate[2], i, j);
        end;
    end;
    WriteLn('max element below main diagonal =', maxElement, ' i=', coordinate[1], ' j=', coordinate[2]);
end;

procedure ReplaceMatrixElements(var matrix: MatrixType; var coordinate1, coordinate2: Coordinates);
var element: Integer;
begin
    element := matrix[coordinate1[1], coordinate1[2]];
    matrix[coordinate1[1], coordinate1[2]]:= matrix[coordinate2[1], coordinate2[2]];
    matrix[coordinate2[1], coordinate2[2]]:= element;
end;

function FindMaxElementWholeMatrix(var matrix: MatrixType): Integer;
var
    i, j, maximum: Integer;
begin
    maximum := matrix[1][1];
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            if matrix[i][j] > maximum then
            begin
                maximum := matrix[i][j];
            end;
        end;
    end;
    WriteLn( 'max element of whole matrix = ', maximum);
    FindMaxElementWholeMatrix := maximum;
end;

function FindMinElementWholeMatrix(var matrix: MatrixType): Integer;
var
    i, j, minimum: Integer;
begin
    minimum := matrix[1][1];
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            if matrix[i][j] < minimum then
            begin
                minimum := matrix[i][j];
            end;
        end;
    end;
    WriteLn( 'min element of whole matrix = ', minimum);
    FindMinElementWholeMatrix := minimum;
end;

procedure SumMinMaxElement(var matrix: MatrixType);
var sum: Integer;
begin
    sum := FindMaxElementWholeMatrix(matrix) + FindMinElementWholeMatrix(matrix);
    WriteLn('min+max=',sum);
end;

procedure ReplaceElement(var element: Integer);
begin
    if (element <= 0) and (element <> 0)then
    begin
        Write('    -');
    end
    else
    begin
        if  element = 0 then
        begin
            Write('    0');
        end
        else
        begin
            Write('    +');
        end;
    end;
end;

procedure ChangeElementsMatrix(var matrix: MatrixType);
var i, j: Integer;
begin
    WriteLn;
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            ReplaceElement(matrix[i, j]);
        end;
        Writeln;
    end;
end;

var
    matrix: MatrixType;
    elementCoordinateOnDiagonal, elementCoordinateBelowDiagonal: Coordinates;


begin
    Randomize;
    CreateMatrix(matrix);
    WriteLn('Matrix :');
    WriteMatrix(matrix);

    FindMaxElementOnMainDiagonal(matrix, elementCoordinateOnDiagonal);
    FindMaxElementBelowMainDiagonal(matrix, elementCoordinateBelowDiagonal);
    ReplaceMatrixElements(matrix, elementCoordinateOnDiagonal, elementCoordinateBelowDiagonal);
    WriteLn;
    WriteLn('Changed matrix:');
    WriteMatrix(matrix);

    WriteLn('matrix + - 0 ');
    ChangeElementsMatrix(matrix);

    SumMinMaxElement(matrix);
    ReadLn;

end.



